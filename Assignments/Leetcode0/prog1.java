/*
 Given a signed 32-bit integer X,return x with its digit reversed. if reversing x causes the value to go outside the 32-bit integer range[-231,231-1],then return 0;

 Assume the enviroment does not allow you to store 64-bit integer(signed or unsigned)

input:x = 123
output: 321

 */

import java.io.*;
class ReverseInteger{
	static int ReverseInteger(int num){
		if(num<= 1){
			return num;
		}
			return Integer.parseInt(num % 10  + "" + ReverseInteger(num / 10));
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int num = Integer.parseInt(br.readLine());
		if(num<0){
			System.out.println("number :-"+(ReverseInteger(-num)));
		}
		else{
			System.out.println("number :"+(ReverseInteger(num)));
		}

	}
}
