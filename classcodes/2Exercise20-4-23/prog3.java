import java.io.*;
class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int n=Integer.parseInt(br.readLine());
		int arr[]=new int[n];
		int Esum=0;
		int Osum=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				Esum=Esum+arr[i];
			}
			else{
				Osum=Osum+arr[i];
			}
		}
		System.out.println("Even sum= " +Esum);
		System.out.println("Odd sum= " +Osum);	
		
	}
}
