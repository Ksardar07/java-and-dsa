class MyThread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup pthreadGP=new ThreadGroup("India");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();
		
		Thread t1 = new Thread(pthreadGP,obj1,"maharashtar");
		Thread t2 = new Thread(pthreadGP,obj2,"Goa");

		t1.start();
		t2.start();

		ThreadGroup cthreadGP=new ThreadGroup(pthreadGP,"pakistan");

		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();
		
		Thread t3 = new Thread(pthreadGP,obj3,"lahore");
		Thread t4 = new Thread(pthreadGP,obj4,"Karachi");

		t3.start();
		t4.start();

		System.out.println(pthreadGP.activeCount());
		System.out.println(pthreadGP.activeGroupCount());
		System.out.println(cthreadGP.activeCount());
	}
}
