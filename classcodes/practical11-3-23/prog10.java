class marksheet{
	public static void main(String args[]){
		int s1=45;
		int s2=50;
		int s3=49;
		double av;
		int tot;
		tot=s1+s2+s3;
		av=tot/3;
		if(av>=75){
			System.out.println("distinction");
		}
		else if(av>=60 && av<75){
			System.out.println("1 class");
		}
		else if(av>=45 && av<60){
			System.out.println("2 class");
		}
		else if(av>=35 && av<45){
			System.out.println("3 class");
		}
		else{
			System.out.println("fail");
		}
	}
}
