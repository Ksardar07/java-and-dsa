import java.io.*;
class JaggedArray{
	public static void main(String args[])throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
		//System.out.println("Enter the column: ");
		//int col = Integer.parseInt(br.readLine()); 
		
		//System.out.println("Enter columns value for each row: ");	
		int arr[][]=new int[row][];
		for(int i=0;i<row;i++){
			System.out.println("Enter columns value : ");
			arr[i]=new int[Integer.parseInt(br.readLine())];
		}


					
		//arr[0]=new int[3];
		//arr[1]=new int[2];
		//arr[2]=new int[1];
		System.out.println("Enter array elements= ");
		
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		//System.out.println(arr[0][2]);
		System.out.println("Array");
		for(int[] x:arr){
			for(int y:x){
				System.out.print(y+ "\t");
			}
			System.out.println();
		}
		//System.out.println(arr[0][2]);

	}
}
