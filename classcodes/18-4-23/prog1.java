class ArrayDemo{
	public static void main(String args[]){
		int arr[]=new int[4];
		arr[0]=10;
		arr[1]=20;
		arr[2]=30;
		arr[3]=40;

		int arr1[]={10,30,50,70};
		int arr2[]=new int[]{30,20,10,0};

		//Error
		int arr3[]=new int[4]{70,80,90,100};
	}
}

