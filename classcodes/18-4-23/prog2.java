import java.io.*;
class RCB{
	public static void main(String args[])throws ArrayIndexOutOfBoundsException{
		int arr1[]={10,20,30,40,50};
		char arr2[]={'a','b','c'};
		float arr3[]={10.3f,56.4f};
		boolean arr4[]={true,false,true};

		//Integer Array
		System.out.println(arr1[0]);
		System.out.println(arr1[1]);
		System.out.println(arr1[2]);
		System.out.println(arr1[3]);
		System.out.println(arr1[4]);

		//character Array
		System.out.println(arr2[0]);
		System.out.println(arr2[1]);
		System.out.println(arr2[2]);

		//float Array
		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
		//System.out.println(arr3[2]);
		//boolean Array
		System.out.println(arr4[0]);
		System.out.println(arr4[1]);
		System.out.println(arr4[2]);
	}
}
