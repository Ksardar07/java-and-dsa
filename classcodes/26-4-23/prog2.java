class ArrayDemo{
	public static void main(String args[]){
		String str[]={"kunal","mitesh","mithil"};
		System.out.println(System.identityHashCode(str[0]));
		System.out.println(System.identityHashCode(str[1]));
		System.out.println(System.identityHashCode(str[2]));

		for(String x:str){
			System.out.println(x);
		}
		
		System.out.println(System.identityHashCode(args[0]));
		System.out.println(System.identityHashCode(args[1]));
	}
}
