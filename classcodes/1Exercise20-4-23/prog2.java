import java.io.*;
class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		int prodt=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				prodt=prodt*arr[i];
			}
		}
		System.out.println("product of even= "+ prodt);
	}
}

