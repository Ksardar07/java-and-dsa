 import java.io.*;
 class patternDemo{
	 public static void main(String args[])throws IOException{
		 BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		 int row=Integer.parseInt(br.readLine());
		 int n=row*2+2;
		 int ch=64+n;

		 for(int i=1;i<=row;i++){
			 for(int j=1;j<=i;j++){
				 if(i%2==0){
					 System.out.print((char)ch+ " ");
				 }
				 else{
					 System.out.print(n+" ");
				 }
				 n--;
				 ch--;
			 }
			 System.out.println();
		 }
	 }
 }

