import java.io.*;
class patternDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int var=row-i+1;
			for(int j=1;j<=row+1-i;j++){
				System.out.print(var*i +" ");
				var=var-1;
			}
			System.out.println();
		}
	}
}

