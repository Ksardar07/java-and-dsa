import java.util.Scanner;
class DataOverFlowException extends RuntimeException{
	DataOverFlowException(String msg){
		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException{
	DataUnderFlowException(String msg){
		super(msg);
	}
}
class ArrayDemo{
	public static void main(String[] args){
		//int arr[]=new arr[5];
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter only integer value");
		System.out.println("Note: 0<element<100");
		int arr[]=new int[5];
		for(int i=0;i<arr.length;i++){
			int data=sc.nextInt();
			if(data<0)
				throw new DataUnderFlowException("mitra data 'o' peksha lahan ahe");
			if(data>100)
				throw new DataOverFlowException("mitra data '100' peksha jast ahe");
			arr[i]=data;
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+ " ");
		}
	}
}

