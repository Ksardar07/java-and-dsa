/*
   find the shortest subarray containning boyh min and max values

   arr= [1,2,3,1,3,4,6,4,6,3]
   -return the length of the smallest subarray
   which contain both th maximum of the array and minimum of the array

 */


class subArray{
	public static void main(String[] args){
		int arr[] = new int[]{1,2,3,1,3,4,6,4,6,3};
		int minlength = Integer.MAX_VALUE;
		int maxele = Integer.MIN_VALUE;
		int minele = Integer.MAX_VALUE;
		int len = 0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]<minele){
				minele = arr[i];
			
				if(arr[i]>maxele)
					maxele = arr[i];
			}
			if(arr[i] == minele){
				for(int j=i+1; j<arr.length; j++){
					if(arr[j] == maxele){
						len = j-i+1;
						if(minlength>len)
							minlength=len;
					}
				}
			}
		}
		System.out.println(minlength);
	}
}

