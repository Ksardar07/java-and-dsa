class Demo{
	int x=10;
	Demo(){
		this(70);
		System.out.println("In No-args constructor");
	}
	Demo(int x){
		this();
		System.out.println("In para constructor");
	}
	public static void main(String[] args){
		Demo obj1=new Demo(50);

	}
}
