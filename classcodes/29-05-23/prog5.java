class Demo{
	int x=10;
	Demo(){
		this(40);
		System.out.println("In no-args constructor ");
	}
	Demo(int x){
		super();
		System.out.println("In para constructor");
	}
	public static void main(String[] args){
		Demo obj1=new Demo();
	}
}


